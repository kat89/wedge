package io.ionic.keyboard;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import java.util.concurrent.ExecutorService;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;
import org.json.JSONArray;
import org.json.JSONException;

public class IonicKeyboard
  extends CordovaPlugin
{
  public boolean execute(String paramString, JSONArray paramJSONArray, final CallbackContext paramCallbackContext)
    throws JSONException
  {
    if ("close".equals(paramString))
    {
      this.cordova.getThreadPool().execute(new Runnable()
      {
        public void run()
        {
          InputMethodManager localInputMethodManager = (InputMethodManager)IonicKeyboard.this.cordova.getActivity().getSystemService("input_method");
          View localView = IonicKeyboard.this.cordova.getActivity().getCurrentFocus();
          if (localView == null)
          {
            paramCallbackContext.error("No current focus");
            return;
          }
          localInputMethodManager.hideSoftInputFromWindow(localView.getWindowToken(), 2);
          paramCallbackContext.success();
        }
      });
      return true;
    }
    if ("show".equals(paramString))
    {
      this.cordova.getThreadPool().execute(new Runnable()
      {
        public void run()
        {
          ((InputMethodManager)IonicKeyboard.this.cordova.getActivity().getSystemService("input_method")).toggleSoftInput(0, 1);
          paramCallbackContext.success();
        }
      });
      return true;
    }
    if ("init".equals(paramString))
    {
      this.cordova.getThreadPool().execute(new Runnable()
      {
        public void run()
        {
          DisplayMetrics localDisplayMetrics = new DisplayMetrics();
          IonicKeyboard.this.cordova.getActivity().getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
          final float f = localDisplayMetrics.density;
          final View localView = IonicKeyboard.this.cordova.getActivity().getWindow().getDecorView().findViewById(16908290).getRootView();
          ViewTreeObserver.OnGlobalLayoutListener local1 = new ViewTreeObserver.OnGlobalLayoutListener()
          {
            int previousHeightDiff = 0;
            
            public void onGlobalLayout()
            {
              Rect localRect = new Rect();
              localView.getWindowVisibleDisplayFrame(localRect);
              int i = localView.getRootView().getHeight();
              int j = localRect.bottom;
              int k;
              int m;
              if (Build.VERSION.SDK_INT >= 21)
              {
                Display localDisplay = IonicKeyboard.this.cordova.getActivity().getWindowManager().getDefaultDisplay();
                Point localPoint = new Point();
                localDisplay.getSize(localPoint);
                k = localPoint.y;
                m = (int)((k - j) / f);
                if ((m <= 100) || (m == this.previousHeightDiff)) {
                  break label187;
                }
                String str = "S" + Integer.toString(m);
                PluginResult localPluginResult2 = new PluginResult(PluginResult.Status.OK, str);
                localPluginResult2.setKeepCallback(true);
                IonicKeyboard.3.this.val$callbackContext.sendPluginResult(localPluginResult2);
              }
              for (;;)
              {
                this.previousHeightDiff = m;
                return;
                k = i;
                break;
                label187:
                if ((m != this.previousHeightDiff) && (this.previousHeightDiff - m > 100))
                {
                  PluginResult localPluginResult1 = new PluginResult(PluginResult.Status.OK, "H");
                  localPluginResult1.setKeepCallback(true);
                  IonicKeyboard.3.this.val$callbackContext.sendPluginResult(localPluginResult1);
                }
              }
            }
          };
          localView.getViewTreeObserver().addOnGlobalLayoutListener(local1);
          PluginResult localPluginResult = new PluginResult(PluginResult.Status.OK);
          localPluginResult.setKeepCallback(true);
          paramCallbackContext.sendPluginResult(localPluginResult);
        }
      });
      return true;
    }
    return false;
  }
  
  public void initialize(CordovaInterface paramCordovaInterface, CordovaWebView paramCordovaWebView)
  {
    super.initialize(paramCordovaInterface, paramCordovaWebView);
  }
}


/* Location:              C:\Users\kat\Documents\WedgeDecompile\dex2jar-0.0.9.15\classes-dex2jar.jar!\io\ionic\keyboard\IonicKeyboard.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */