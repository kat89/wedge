package org.apache.cordova.statusbar;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build.VERSION;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import java.lang.reflect.Method;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaPreferences;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.LOG;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;
import org.json.JSONException;

public class StatusBar
  extends CordovaPlugin
{
  private static final String TAG = "StatusBar";
  
  private void setStatusBarBackgroundColor(String paramString)
  {
    Window localWindow;
    if ((Build.VERSION.SDK_INT >= 21) && (paramString != null) && (!paramString.isEmpty()))
    {
      localWindow = this.cordova.getActivity().getWindow();
      localWindow.clearFlags(67108864);
      localWindow.addFlags(Integer.MIN_VALUE);
    }
    try
    {
      Class localClass = localWindow.getClass();
      Class[] arrayOfClass = new Class[1];
      arrayOfClass[0] = Integer.TYPE;
      Method localMethod = localClass.getDeclaredMethod("setStatusBarColor", arrayOfClass);
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = Integer.valueOf(Color.parseColor(paramString));
      localMethod.invoke(localWindow, arrayOfObject);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      LOG.e("StatusBar", "Invalid hexString argument, use f.i. '#999999'");
      return;
    }
    catch (Exception localException)
    {
      LOG.w("StatusBar", "Method window.setStatusBarColor not found for SDK level " + Build.VERSION.SDK_INT);
    }
  }
  
  public boolean execute(String paramString, final CordovaArgs paramCordovaArgs, CallbackContext paramCallbackContext)
    throws JSONException
  {
    LOG.v("StatusBar", "Executing action: " + paramString);
    final Window localWindow = this.cordova.getActivity().getWindow();
    if ("_ready".equals(paramString))
    {
      int i = 0x400 & localWindow.getAttributes().flags;
      boolean bool = false;
      if (i == 0) {
        bool = true;
      }
      paramCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, bool));
      return true;
    }
    if ("show".equals(paramString))
    {
      this.cordova.getActivity().runOnUiThread(new Runnable()
      {
        public void run()
        {
          if (Build.VERSION.SDK_INT >= 19)
          {
            int i = 0xFFFFFFFB & 0xFBFF & localWindow.getDecorView().getSystemUiVisibility();
            localWindow.getDecorView().setSystemUiVisibility(i);
          }
          localWindow.clearFlags(1024);
        }
      });
      return true;
    }
    if ("hide".equals(paramString))
    {
      this.cordova.getActivity().runOnUiThread(new Runnable()
      {
        public void run()
        {
          if (Build.VERSION.SDK_INT >= 19)
          {
            int i = 0x4 | 0x400 | localWindow.getDecorView().getSystemUiVisibility();
            localWindow.getDecorView().setSystemUiVisibility(i);
          }
          localWindow.addFlags(1024);
        }
      });
      return true;
    }
    if ("backgroundColorByHexString".equals(paramString))
    {
      this.cordova.getActivity().runOnUiThread(new Runnable()
      {
        public void run()
        {
          try
          {
            StatusBar.this.setStatusBarBackgroundColor(paramCordovaArgs.getString(0));
            return;
          }
          catch (JSONException localJSONException)
          {
            LOG.e("StatusBar", "Invalid hexString argument, use f.i. '#777777'");
          }
        }
      });
      return true;
    }
    return false;
  }
  
  public void initialize(final CordovaInterface paramCordovaInterface, CordovaWebView paramCordovaWebView)
  {
    LOG.v("StatusBar", "StatusBar: initialization");
    super.initialize(paramCordovaInterface, paramCordovaWebView);
    this.cordova.getActivity().runOnUiThread(new Runnable()
    {
      public void run()
      {
        paramCordovaInterface.getActivity().getWindow().clearFlags(2048);
        StatusBar.this.setStatusBarBackgroundColor(StatusBar.this.preferences.getString("StatusBarBackgroundColor", "#000000"));
      }
    });
  }
}


/* Location:              C:\Users\kat\Documents\WedgeDecompile\dex2jar-0.0.9.15\classes-dex2jar.jar!\org\apache\cordova\statusbar\StatusBar.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */