package com.ionicframework.wedge613137;

import android.os.Bundle;
import org.apache.cordova.CordovaActivity;

public class MainActivity
  extends CordovaActivity
{
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    loadUrl(this.launchUrl);
  }
}


/* Location:              C:\Users\kat\Documents\WedgeDecompile\dex2jar-0.0.9.15\classes-dex2jar.jar!\com\ionicframework\wedge613137\MainActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */